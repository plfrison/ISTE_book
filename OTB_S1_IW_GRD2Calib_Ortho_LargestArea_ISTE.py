##ISTE: Sentinel-1 IW Batch Processing=group
##1a - GRD Prod. Cal. and Orthorect. over Largest Area=name
##Input_Data_Folder=folder
##DEM_Folder=folder
##Calibration_Type=string Sigma0
##Output_EPSG=crs
##Output_Resolution=number 10
##Output_Data_Folder=folder
##Ram=number 256

'''
Date:  Juillet 2017
Auteurs:
Cedric Lardeux: clardeux_at_gmail.com cedric.lardeux_at_onfinternational.com
Pierre-Louis Frison: pierre-louis.frison_at_u-pem.fr
'''

import sys, shutil
import glob, os, zipfile, os.path
import string
import subprocess
from datetime import datetime, date, time
from osgeo import ogr
from scipy.ndimage.filters import uniform_filter
from osgeo import gdal
import numpy as np

# This script is dedicated to orthorectified Sentinel-1 data in GRD IW mode and dual pol (VV, VH or HH, VH)
# It's based on OTB 5.6

Clip_With_Vector= False
Download_SRTM=False
DataFolder = Input_Data_Folder
OutputFolder = Output_Data_Folder

Output_in_dB= True

'''
INPUT
'''
Noise = 'false'
if Calibration_Type == 'Sigma0':
	Calibration_Type = 'sigma'
	Calib_Name='Sig0'
elif Calibration_Type == 'Gamma0':
	Calibration_Type = 'gamma'
	Calib_Name='Gam0'
elif Calibration_Type == 'Beta0':
	Calibration_Type = 'beta'
	Calib_Name='Bet0'
else:
	Calibration_Type='sigma'
	Calib_Name='Sig0'

DemDir = DEM_Folder


def TileRasterProcessingAverage(aInputRasterListPath, aOutputRaster, aBlockSize):
    '''
    TODO: Add sliding window parameter


    This method show how to process huge raster
    using easy tilling based on block size
    and if necessary taking into account window size
    when necessary in processing like filtering for example
    aInputRasterPath string --> Contain input raster path to process
    aOutputRasterPath string --> Contain output raster path to process
    aBlockSize integer --> Contain the size of square block to Tile
    aWindowSize integer --> Contain windows size for sliding window (0 size by default)
    '''
    aWindowSize = 0

    # we put X and Y block size based on aBlockSize
    xBSize = aBlockSize
    yBSize = aBlockSize


    NumBands = len(aInputRasterListPath)

    # We open one raster to get rows and cols
    src_ds = gdal.Open( aInputRasterListPath[0] )
    if src_ds is None:
        print 'Could not open ' + fn
        sys.exit(1)

    # We get number of row and cols
    rows = src_ds.RasterYSize
    cols = src_ds.RasterXSize

    # We force Float32 to read data
    BandType = gdal.GDT_Float32

    # We get Projection from input raster
    InputGeoTransform = src_ds.GetGeoTransform()
    InputProjection = src_ds.GetProjection()

    src_ds = None

    # Open input files
    GdalFilePointerList = []
    GdalOutputFilePointerList = []
    for i in range(NumBands):
        GdalFilePointerList.append(gdal.Open( aInputRasterListPath[i] ))

    # Output file
    format = "GTiff"
    driver = gdal.GetDriverByName( format )
    dst_raster = driver.Create(aOutputRaster, cols, rows, 1, BandType )
    dst_raster.SetGeoTransform(InputGeoTransform)
    dst_raster.SetProjection(InputProjection)

    BlockId = 0
    for i in range(0, rows, yBSize):
        if i + yBSize < rows:
            numRows = yBSize
        else:
            numRows = rows - i
        # Backup i and numRows
        numRowsDefault = numRows
        iOriginal = i
        for j in range(0, cols, xBSize):
            i = iOriginal
            numRows = numRowsDefault
            print '\n Block numero : ', BlockId
            if j + xBSize < cols:
                numCols = xBSize
            else:
                numCols = cols - j
            numColsDefault = numCols

            # Test for applying sliding window buffer
            # Backup j
            jOriginal = j

            iOffset = 0
            jOffset = 0
            numColsOffset = 0
            numRowsOffset = 0
            if i - aWindowSize >= 0:
                i = i - aWindowSize
                iOffset = aWindowSize
            if j - aWindowSize >= 0:
                j = j - aWindowSize
                jOffset = aWindowSize

            if jOriginal + numCols + aWindowSize <= cols:
                numCols = numCols + aWindowSize
                numColsOffset = aWindowSize
            numCols = numCols + jOffset

            if iOriginal + numRows + aWindowSize <= rows:
                numRows = numRows + aWindowSize
                numRowsOffset = aWindowSize
            numRows = numRows + iOffset

            Data = np.zeros(shape=(NumBands, numRows, numCols ),dtype=np.float)

            #We get file values
            for it in range(len(GdalFilePointerList)):
                GetBand = GdalFilePointerList[it].GetRasterBand(1)
                Data[it, :,:] = GetBand.ReadAsArray(j, i, numCols, numRows)
            '''

            Do something like

            '''
            # Temporal average
            Mean = np.mean(Data, axis=0)

            print 'np.shape(Data)',np.shape(Data)
            print 'np.shape(Mean)',np.shape(Mean)

            #Clip the border
            Mean = Mean[iOffset:iOffset + numRowsDefault,jOffset:jOffset + numColsDefault]

            print 'np.shape(Mean)',np.shape(Mean)

            #We writte Quegan filtered data
            band = dst_raster.GetRasterBand(1)
            band.SetNoDataValue(0)
            band.WriteArray(Mean,jOriginal,iOriginal)

            BlockId = BlockId + 1

    # We close all file
    src_ds = None

def Int2dB(InputFile, Band, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += InputFile + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\"" + Band + "<=0.001 ? -30.0 : 10 * log10(" +Band +")\""

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

# OTB Create HVdB - HHdB
def DiffdB(aInputFileCopol, aInputFileCrosspol, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += aInputFileCopol + " "
	cmd += aInputFileCrosspol + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\" im2b1 - im1b1 \""

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

# OTB ratio
def RatioDualPol(aInputFileCopol, aInputFileCrosspol, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += aInputFileCopol + " "
	cmd += aInputFileCrosspol + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\" im2b1 / im1b1 \""

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GenerateDualPolColorcompositiondB(aCopolFile, aCrosspolFile):
	CopoldBFile = aCopolFile.replace('VV_', 'VVdB_')
	CrosspoldBFile = aCopolFile.replace('VV_', 'VHdB_')

	DiffFile = aCopolFile.replace('VV_', 'VH-VV_dB_')

	OutputColorCompVRTFile = aCopolFile.replace('VV_',  'VV_VH_VH-VV_dB_')
	OutputColorCompVRTFile = OutputColorCompVRTFile.replace('.tif',  '.vrt')

	Int2dB(aCopolFile, 'im1b1', CopoldBFile, Ram)
	Int2dB(aCrosspolFile, 'im1b1', CrosspoldBFile, Ram)
	DiffdB(CopoldBFile, CrosspoldBFile, DiffFile, Ram)

	# Del IntCopol
	if os.path.exists(aCopolFile):
			os.remove(aCopolFile)

	# Del IntCrosspol
	if os.path.exists(aCrosspolFile):
			os.remove(aCrosspolFile)

	# VRT color composition
	GdalBuildVRT([CopoldBFile, CrosspoldBFile, DiffFile], OutputColorCompVRTFile,  0, 0)


def GenerateDualPolColorcompositionInt(aCopolFile, aCrosspolFile):
	Ratio = aCopolFile.replace('VV_', 'VH-VV_')
	print Ratio

	OutputColorCompVRTFile = aCopolFile.replace('VV_',  'VV_VH_VH-VV_')
	OutputColorCompVRTFile = OutputColorCompVRTFile.replace('.tif',  '.vrt')

	print OutputColorCompVRTFile

	RatioDualPol(aCopolFile, aCrosspolFile, Ratio, Ram)

	GdalBuildVRT([aCopolFile, aCrosspolFile, Ratio], OutputColorCompVRTFile,  0, 0)

def getDayFromS1File(aFileName):
	aFileName = aFileName.split("/")[-1]
	return aFileName.split("_")[1][0:8]

def GdalBuildVRT(aInputListFile, aOutputFile,  aSRCNoData, aVRTNoData):
	cmd = "gdalbuildvrt "
	cmd += " -separate "
	cmd += " -srcnodata " + str(aSRCNoData)
	cmd += " -vrtnodata " + str(aVRTNoData)
	cmd += " " + aOutputFile
	for File in aInputListFile:
		cmd += " " + File
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GenerateTemporalAverage(aInputFolder):
	AllTifFile = directory_liste_ext_files(aInputFolder, 'tif')
	AllTifFile = [ file for file in AllTifFile if 'Quegan' not in file]
	AllCopolFile = [file for file in AllTifFile if ('VV' in file) or  ('HH' in file)]
	AllCrosspolFile = [file for file in AllTifFile if ('HV' in file) or  ('VH' in file)]


	# We get acquisition date
	ListDates = [getDayFromS1File(RastPath) for RastPath in AllCopolFile ]
	UniqueOutputDates = list(set(ListDates))
	UniqueOutputDates.sort()

	NumDate = len(AllCopolFile)

	BlockSize = int(np.sqrt(float(Ram) * np.power(1024,2) /(4. * 2. *(2. * float(NumDate) + 1.))))

	print Ram, NumDate, BlockSize

	OutputDateSuffix = UniqueOutputDates[0] + '_' + UniqueOutputDates[-1] + '.tif'

	# Apply average to Copol
	OutputCopol = os.path.join(aInputFolder, 'TempAverage_VV_' + OutputDateSuffix)
	TileRasterProcessingAverage(AllCopolFile, OutputCopol, BlockSize)


	# Apply filtering to crosspol
	OutputCrosspol = os.path.join(aInputFolder, 'TempAverage_VH_' + OutputDateSuffix)
	TileRasterProcessingAverage(AllCrosspolFile, OutputCrosspol, BlockSize)

	# Generate ratio intensity or convert in dB and creat vrt for color composition
	# List all folders in Time Filtering forlder
	# Get full path of time filtering folder
	if Output_in_dB:
		GenerateDualPolColorcompositiondB(OutputCopol, OutputCrosspol)
	else:
		GenerateDualPolColorcompositionInt(OutputCopol, OutputCrosspol)


def DelDirAndItsFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
	os.removedirs(aInputDir)

def DelDirFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [os.path.join(a_dir, name) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
	list_file = []
	for root, dirnames, filenames in os.walk(directory):
		for filename in filenames:
			if filename.endswith((filt_ext)):
				list_file.append(os.path.join(root, filename))

	list_file.sort()
	return list_file

'''
This function unzip one given file in a given directory
'''
def unzip(source_filename, dest_dir):
    with zipfile.ZipFile(source_filename) as zf:
        for member in zf.infolist():
            # Path traversal defense copied from
            # http://hg.python.org/cpython/file/tip/Lib/http/server.py#l789
            words = member.filename.split('/')
            path = dest_dir
            for word in words[:-1]:
                while True:
                    drive, word = os.path.splitdrive(word)
                    head, word = os.path.split(word)
                    if not drive:
                        break
                if word in (os.curdir, os.pardir, ''):
                    continue
                path = os.path.join(path, word)
            zf.extract(member, path)

'''
This function calibrate SAR file
aNoise 	--> to remove (or not) noise 	- True or False
aCalibration_Type 	--> to chose aCalibration_Type 		- sigma/gamma/beta/dn
'''
def OTBSARCalibration(aInputFile, aOutputFile, aNoise, aCalibration_Type, aRam):
	cmd = "otbcli_SARCalibration -in "
	cmd += aInputFile + " "
	cmd += " -ram " + str(aRam)
	cmd += " -out "
	cmd += aOutputFile
	cmd += " -noise " + aNoise
	cmd += " -lut " + aCalibration_Type

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

'''
This function do orthorectification of any optical/radar file
aDEMDir 	--> directory that contain DEM

opt.gridspacing is not in parameter function but need to be near 4 to 10 times of output pixel size
For example, with 10m output pixel size, you can chose bettween 40 to 100.
More less is the value more accurate is the results but more long
In addition this value have to be linked to DEM pixel size
'''
def OTBOrthorectification(aInputFile, aOutputFile, aDEMDir , aRam):
	cmd = "otbcli_OrthoRectification -io.in "
	cmd += aInputFile + " "
	cmd += " -opt.ram " + str(aRam)
	cmd += " -io.out "
	cmd += aOutputFile
	cmd += " -elev.dem " + aDEMDir
	cmd += " -opt.gridspacing 40 "

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def OTBExtractRoi(aInputFile, aInputRefFile, aOutputFile, aRam):
	cmd = "otbcli_ExtractROI -in "
	cmd += aInputFile + " "
	cmd += " -ram " + str(aRam)
	cmd += " -out "
	cmd += aOutputFile
	cmd += " -mode fit "
	cmd += " -mode.fit.ref " + aInputRefFile

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def ReprojVector(aInputVector, aOutputVector, aEPSG):
	cmd = "ogr2ogr "
	cmd += " -f \"ESRI Shapefile\"  " + aOutputVector+ " "
	cmd +=  aInputVector  + " "
	cmd += " -t_srs EPSG:" + str(aEPSG)

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def Rasterize(aInputVector, aOutputRaster, aOutPixelSize):
	# Multilook
	cmd = "gdal_rasterize "
	cmd += " -burn 1 "
	cmd += " -of GTiff "
	cmd += " -a_nodata 0 "
	cmd += " -a_srs EPSG:3857 "
	cmd += " -tr " + str(aOutPixelSize) + " "+ str(aOutPixelSize)+ " "
	cmd += aInputVector + " "
	cmd += aOutputRaster

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

# OTB
def OTBDownloadSRTMTile(aInputFile, aDEMDir):
	cmd = "otbcli_DownloadSRTMTiles -il "
	cmd += aInputFile + " "
	cmd += " -mode.download.outdir " + aDEMDir

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GRD2Calib_Ortho(aInputFile,aOutputFile, aCalibration_Type, aNoise, aRam):
	TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

	# Output Folder Name
	OutputFolderName = os.path.dirname(aOutputFile)

	# Output Intermediate Calibrated file
	OutputCalibratedFile = os.path.join(TmpDirGarbage, TimeNow + "_Calib.tif")

	# Calibration
	OTBSARCalibration(aInputFile, OutputCalibratedFile, aNoise, aCalibration_Type, aRam)

	# Output DEM Dir
	if Download_SRTM:
		OutputDEMDir = os.path.join(DemDir, 'Download' )
	else:
		OutputDEMDir = DemDir

	# Download SRTM DEM
	if not os.path.exists(OutputDEMDir):
	    os.makedirs(OutputDEMDir)

	if Download_SRTM:
		OTBDownloadSRTMTile(OutputCalibratedFile, OutputDEMDir)
		ZipDemFiles = directory_liste_ext_files(OutputDEMDir, '.zip')

		for zipFile in ZipDemFiles:
			unzip(zipFile, OutputDEMDir)

	# Orthorectification
	OTBOrthorectification(OutputCalibratedFile, aOutputFile, OutputDEMDir, aRam)


def Polygonize(aInputRaster, aOutputVector):
	# Multilook
	if 'nt' in os.name:
		cmd = "gdal_polygonize.bat   "
	else:
		cmd = "gdal_polygonize.py   "

	cmd += aInputRaster + " "
	cmd += " -mask " + aInputRaster
	cmd += " -b 1 -f \"ESRI Shapefile\" "
	cmd += aOutputVector

	# progress.setInfo('\nPolygonize ' + cmd)
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GdalClipRasterWithVector(aInputRaster, aInputVector, aOutputFile, aOutputResolution, aSRCNoData, aOutEPSG):
	cmd = "gdalwarp "
	cmd += " -tr " + str(aOutputResolution) + ' ' + str(aOutputResolution) + ' '
	cmd += " -t_srs " + str(aOutEPSG)
	cmd += " -q -multi -crop_to_cutline -cutline "
	cmd += aInputVector
	cmd += " -dstnodata " + str(aSRCNoData)
	cmd += " -of GTiff " + aInputRaster + ' ' + aOutputFile

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]


def GdalVrtStacking(aListFile, aOutputRaster, aNodataVal, aSeparate):
    cmd = 'gdalbuildvrt '
    if aSeparate: cmd += '-separate '
    cmd += ' -resolution highest '
    cmd += ' -srcnodata ' + str(aNodataVal) + ' -vrtnodata '+ str(aNodataVal) + ' '
    cmd += '-input_file_list ' + aListFile + ' -overwrite '
    cmd += aOutputRaster

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

def GdalVrtSplitBands(aInputRaster):
    InputRasterName = os.path.splitext(os.path.basename(aInputRaster))[0]
    InputRasterFolder = os.path.dirname(aInputRaster)

    print aInputRaster

    ds=gdal.Open(aInputRaster)
    bands = ds.RasterCount
    ds = None

    OutListFiles = []
    for band in range(bands):
        OutFile = os.path.join(InputRasterFolder,InputRasterName + '_b'+str(band+1)+'.vrt')
        OutListFiles.append(OutFile)

        cmd = 'gdalbuildvrt '
        cmd += '-srcnodata 0 -vrtnodata 0 '
        cmd += " -b " + str(band + 1) + ' '
        cmd += OutFile + ' '
        cmd += aInputRaster + ' '

        p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
        ret= p1.communicate()[1]

    return OutListFiles

def OTBProduct(aInputRasterList, aOutputRaster, aRam):
	NumBand = len(aInputRasterList)
	cmd = "otbcli_BandMath -il "
	for File in aInputRasterList:
		cmd += " " + File
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += aOutputRaster
	cmd += " -exp "
	cmd += "\"("
	for i in range(NumBand):
		cmd += " " + "im" + str(i+1) + 'b1*'
	cmd = cmd[:-1]
	cmd += " )>0\""

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]
def ConvexHull(aInputShape, aOutputShape):


	# Get a Layer
	inDriver = ogr.GetDriverByName("ESRI Shapefile")
	inDataSource = inDriver.Open(aInputShape, 0)
	inLayer = inDataSource.GetLayer()

	# prj name of shape
	outPRJfile = aOutputShape.replace('.shp', '.prj')

	#get spatial ref
	spatialRef = inLayer.GetSpatialRef()

	# Collect all Geometry
	geomcol = ogr.Geometry(ogr.wkbGeometryCollection)
	for feature in inLayer:
	    geomcol.AddGeometry(feature.GetGeometryRef())

	# Calculate convex hull
	convexhull = geomcol.ConvexHull()

	# Save extent to a new Shapefile
	outDriver = ogr.GetDriverByName("ESRI Shapefile")

	# Remove output shapefile if it already exists
	if os.path.exists(aOutputShape):
	    outDriver.DeleteDataSource(aOutputShape)

	# Create the output shapefile
	outDataSource = outDriver.CreateDataSource(aOutputShape)
	outLayer = outDataSource.CreateLayer("states_convexhull", geom_type=ogr.wkbPolygon)

	# Add an ID field
	idField = ogr.FieldDefn("id", ogr.OFTInteger)
	outLayer.CreateField(idField)

	# Create the feature and set values
	featureDefn = outLayer.GetLayerDefn()
	feature = ogr.Feature(featureDefn)
	feature.SetGeometry(convexhull)
	feature.SetField("id", 1)
	outLayer.CreateFeature(feature)

	spatialRef.MorphToESRI()
	file = open(outPRJfile, 'w')
	file.write(spatialRef.ExportToWkt())
	file.close()

	feature = None

	# Save and close DataSource
	inDataSource = None
	outDataSource = None

def RasterFactorResampling(aInputRaster, aOutput_Raster_File, aRaster_Scale_Factor, aNodata):
	src_ds = gdal.Open( aInputRaster )
	if src_ds is None:
		print 'Could not open ' + fn
		sys.exit(1)
	else:
		# Get raster information
		cols = src_ds.RasterXSize
		rows = src_ds.RasterYSize

		#lecture de la taille de pixel
		cols_out = int(cols / aRaster_Scale_Factor)
		rows_out = int(rows / aRaster_Scale_Factor)

		# reprojection
		print ""
		cmd = "gdalwarp  -dstnodata "
		cmd += str(aNodata)
		cmd += " -multi "
		cmd += " -r average -of GTiff "
		cmd += " -ts " + str(cols_out) + " " + str(rows_out) + " "
		cmd += aInputRaster
		cmd += " "
		cmd += aOutput_Raster_File

		p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
		ret= p1.communicate()[1]
	src_ds = None

def CreateSmallestOverlap(aInputRasterList, aOutputShape, aRam):
	DirShape = os.path.dirname(aOutputShape)
	TmpDir = os.path.join(DirShape,datetime.now().strftime("%Y_%m_%d_%H_%M_%S"))

	if not os.path.exists(TmpDir):
		os.makedirs(TmpDir)
	
	# Create multiband vrt to do the stack (use resolution and epsg)
	OutputVRTStack = os.path.join(TmpDir, 'Stack.vrt')
	# Create resample raster
	OutputResampleVRT = os.path.join(TmpDir, 'Stack_ResampleFact10.tif')

	# Filename of intermediate polygone
	OutputTmpPolygone = os.path.join(TmpDir, 'tmpcommonarea.shp')
	# Create txt list file
	OutputListFiles = os.path.join(TmpDir, 'files.txt')
	MyOutputTxt = open(OutputListFiles, "w")

	for file in aInputRasterList:
		 MyOutputTxt.write(file + '\n')
	MyOutputTxt.close()

	GdalVrtStacking(OutputListFiles, OutputVRTStack, str(0), True)

	# Resampling the vrt to go faster
	RasterFactorResampling(OutputVRTStack, OutputResampleVRT, 10, 0)

	# Split the band into vrt to generate mask
	ListBands = GdalVrtSplitBands(OutputResampleVRT)

	# Multiply all vrt to have the raster mask overlap
	OutputRasterMask = os.path.join(TmpDir, 'rastermask.tif')
	OTBProduct(ListBands, OutputRasterMask, aRam)

	# Polygonise the results
	Polygonize(OutputRasterMask, OutputTmpPolygone)

	# Generate convexhull
	ConvexHull(OutputTmpPolygone, aOutputShape)

	# Del TmpOrtho folder
	shutil.rmtree(TmpDir)


'''
Program
'''
# create main tmp dir
TmpDir = os.path.join(OutputFolder, "tmp")
if not os.path.exists(TmpDir):
	os.makedirs(TmpDir)

# Create Garbage Tmp Dir
TmpDirGarbage = os.path.join(TmpDir, "Garbage")
if not os.path.exists(TmpDirGarbage):
	os.makedirs(TmpDirGarbage)

# Create Ortho Tmp Dir
TmpDirOrtho = os.path.join(TmpDir, "Ortho")
if not os.path.exists(TmpDirOrtho):
	os.makedirs(TmpDirOrtho)

# Orthorectification process
# List all SENTINEL-1 sub directories
SubFolders = get_immediate_subdirectories(DataFolder)

S1DataFolders = [ Folder for Folder in SubFolders if '.SAFE' in Folder ]

# Loop thru different S1 data (folder)
for Folder in S1DataFolders:
	TiffFiles = directory_liste_ext_files(Folder, 'tiff')

	SafeName = os.path.split(Folder)[-1]
	FolderName = SafeName.split('.')[0]
	SatelliteFolder=FolderName.split('_')[0]
	DateFolder = FolderName.split('_')[5]

	WorkingFolder = os.path.join(TmpDirOrtho,FolderName)
	if not os.path.exists(WorkingFolder):
		os.makedirs(WorkingFolder)

	OutputFilesOrtho = []
	for TiffFile in TiffFiles:
		if 'grd-vh' in TiffFile:
			SuffixOrtho = '_VH_'

		if 'grd-hv' in TiffFile:
			SuffixOrtho = '_HV_'

		if 'grd-vv' in TiffFile:
			SuffixOrtho = '_VV_'

		if 'grd-hh' in TiffFile:
			SuffixOrtho = '_HH_'

		SuffixOrtho += Calib_Name+'_Ortho.tif'
		OutputFile = os.path.join(TmpDirOrtho,FolderName,FolderName + SuffixOrtho)

		# Do GRD conversion to Ortho (including calibration)
		GRD2Calib_Ortho(TiffFile,OutputFile, Calibration_Type, Noise, Ram)

		# Del Temporay files
		DelDirFiles(TmpDirGarbage)

# Stack Process
# List all SENTINEL-1 sub directories
SubFolders = get_immediate_subdirectories(TmpDirOrtho)

# List all tif file and generate shape footprint
AllTifFile = directory_liste_ext_files(TmpDirOrtho, 'tif')

CommonShapeArea = os.path.join(OutputFolder,'CommonFootprintArea.shp')
CreateSmallestOverlap(AllTifFile, CommonShapeArea, Ram)

# Clip the data
#
# Loop thru different S1 data (folder) to create common shape file footprint
for Folder in SubFolders:
	# List all tif and tiff files
	AllTifFile = directory_liste_ext_files(Folder, 'tif')

	InDirName = os.path.split(Folder)[1]
	SatelliteFolder=FolderName.split('_')[0]
	DateFolder = FolderName.split('_')[5]
	DateFolder=DateFolder[0:DateFolder.find('T')]

	# Create Output subfolder
	OutFolder = os.path.join(OutputFolder,InDirName)
	if not os.path.exists(OutFolder):
		os.makedirs(OutFolder)

	for file in AllTifFile:
		if '_VH_' in file:
			SuffixOrtho = '_VH_'

		if '_HV_' in file:
			SuffixOrtho = '_HV_'

		if '_VV_' in file:
			SuffixOrtho = '_VV_'

		if '_HH_' in file:
			SuffixOrtho = '_HH_'
		
		SuffixOrtho += Calib_Name+'_Clip_Ortho.tif'
		OutputFile = os.path.join(OutFolder, SatelliteFolder + '_' + DateFolder + SuffixOrtho)
		print OutputFile
		GdalClipRasterWithVector(file,CommonShapeArea , OutputFile, Output_Resolution,  0, Output_EPSG)

# Del TmpOrtho folder
shutil.rmtree(TmpDirOrtho)
shutil.rmtree(TmpDir)

GenerateTemporalAverage(OutputFolder)
