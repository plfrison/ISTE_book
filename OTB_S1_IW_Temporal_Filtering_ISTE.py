##ISTE: Sentinel-1 IW Batch Processing=group
##2 - Temporal Filtering=name
##Input_Data_Folder=folder
##Spatial_Window_Size_for_Temporal_Filter=number 7
##Apply_Lee_Pre_Filtering=boolean True
##Spatial_Window_Size_for_Lee_Filter=number 5
##Looks_Number_for_Lee_Filter=number 5
##Output_in_dB=boolean True
##Output_Data_Folder=folder
##Ram=number 256

'''
Date:  Juillet 2017
Auteurs:
Cedric Lardeux: clardeux_at_gmail.com cedric.lardeux_at_onfinternational.com
Pierre-Louis Frison: pierre-louis.frison_at_u-pem.fr
'''

import sys, shutil
import glob, os, zipfile, os.path
import string
import subprocess
from datetime import datetime, date, time
import numpy as np
from scipy.ndimage.filters import uniform_filter
from osgeo import gdal

# This script is dedicated to orthorectified Sentinel-1 data in GRD IW mode and dual pol (VV, VH or HH, VH)
# It's based on OTB 5.6

'''
INPUT
'''
DataFolder = Input_Data_Folder
OutputFolder = Output_Data_Folder
Window_Temp_Filtering=Spatial_Window_Size_for_Temporal_Filter
Window_Size=Spatial_Window_Size_for_Lee_Filter
Radius = int(Window_Size / 2.)
Number_Of_Look=Looks_Number_for_Lee_Filter

def DelDirAndItsFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
	os.removedirs(aInputDir)

def DelDirFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [os.path.join(a_dir, name) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
	list_file = []
	for root, dirnames, filenames in os.walk(directory):
		for filename in filenames:
			if filename.endswith((filt_ext)):
				list_file.append(os.path.join(root, filename))

	list_file.sort()
	return list_file
def ApplyLeeFiltering(aInputFile,aOutputFile, aLeeRadius, aENL, aRam):
	cmd = "otbcli_Despeckle"
	cmd += " -in " + aInputFile
	cmd += " -filter lee"
	cmd += " -filter.lee.rad " + str(aLeeRadius)
	cmd += " -filter.lee.nblooks " + str(aENL)
	cmd += " -ram " + str(aRam)
	cmd += " -out " + aOutputFile

	# progress.setInfo(cmd)

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def QueganTemporalSpeckleFiltering(aTempArray,aWinSize):
    from scipy import ndimage
    eps = 1e-16
    # Jk = E[Ik]/N * Sum (I/E[I])
    NumTime, Rows, Cols = aTempArray.shape

    FiltTempArray = np.zeros(shape=(NumTime, Rows, Cols),dtype=np.float)
    SumPart = np.zeros(shape=(Rows, Cols),dtype=np.float)

    for i in range(NumTime):
        SumPart += aTempArray[i,:,:] / (uniform_filter(aTempArray[i,:,:], size=aWinSize) + eps )

    for i in range(NumTime):
        FiltTempArray[i,:,:] += uniform_filter(aTempArray[i,:,:], size=aWinSize) * SumPart / NumTime

    return FiltTempArray


def TileRasterProcessing(aInputRasterListPath, aOutputRasterListPath, aBlockSize,aTempWindowSize):
    '''
    TODO: Add sliding window parameter


    This method show how to process huge raster
    using easy tilling based on block size
    and if necessary taking into account window size
    when necessary in processing like filtering for example
    aInputRasterPath string --> Contain input raster path to process
    aOutputRasterPath string --> Contain output raster path to process
    aBlockSize integer --> Contain the size of square block to Tile
    aWindowSize integer --> Contain windows size for sliding window (0 size by default)
    '''
    aWindowSize = aTempWindowSize

    # we put X and Y block size based on aBlockSize
    xBSize = aBlockSize
    yBSize = aBlockSize


    NumBands = len(aInputRasterListPath)

    # We open one raster to get rows and cols
    src_ds = gdal.Open( aInputRasterListPath[0] )
    if src_ds is None:
        print 'Could not open ' + fn
        sys.exit(1)

    # We get number of row and cols
    rows = src_ds.RasterYSize
    cols = src_ds.RasterXSize

    # We force Float32 to read data
    BandType = gdal.GDT_Float32

    # We get Projection from input raster
    InputGeoTransform = src_ds.GetGeoTransform()
    InputProjection = src_ds.GetProjection()

    src_ds = None

    # Open input files
    GdalFilePointerList = []
    GdalOutputFilePointerList = []
    for i in range(NumBands):
        GdalFilePointerList.append(gdal.Open( aInputRasterListPath[i] ))

    # Output file
    format = "GTiff"
    driver = gdal.GetDriverByName( format )
    for i in range(NumBands):
        GdalOutputFilePointerList.append(driver.Create(aOutputRasterListPath[i], cols, rows, 1, BandType ))
        GdalOutputFilePointerList[i].SetGeoTransform(InputGeoTransform)
        GdalOutputFilePointerList[i].SetProjection(InputProjection)

    # print 'Rows, Cols: ',rows, cols
    BlockId = 0
    for i in range(0, rows, yBSize):
        if i + yBSize < rows:
            numRows = yBSize
        else:
            numRows = rows - i
        # Backup i and numRows
        numRowsDefault = numRows
        iOriginal = i
        for j in range(0, cols, xBSize):
            i = iOriginal
            numRows = numRowsDefault
            print '\n Block numero : ', BlockId
            if j + xBSize < cols:
                numCols = xBSize
            else:
                numCols = cols - j
            numColsDefault = numCols

            # print 'Indice i,j original : ', i, j
            # print 'numRows numCols :',numRows, numCols
            # Test for applying sliding window buffer
            # Backup j
            jOriginal = j

            iOffset = 0
            jOffset = 0
            numColsOffset = 0
            numRowsOffset = 0
            if i - aWindowSize >= 0:
                i = i - aWindowSize
                iOffset = aWindowSize
            if j - aWindowSize >= 0:
                j = j - aWindowSize
                jOffset = aWindowSize

            if jOriginal + numCols + aWindowSize <= cols:
                numCols = numCols + aWindowSize
                numColsOffset = aWindowSize
            numCols = numCols + jOffset

            if iOriginal + numRows + aWindowSize <= rows:
                numRows = numRows + aWindowSize
                numRowsOffset = aWindowSize
            numRows = numRows + iOffset

            # print 'Read as array j, i, numCols, numRows', j, i, numCols, numRows
            Data = np.zeros(shape=(NumBands, numRows, numCols ),dtype=np.float)

            #We get file values
            for it in range(len(GdalFilePointerList)):
                GetBand = GdalFilePointerList[it].GetRasterBand(1)
                Data[it, :,:] = GetBand.ReadAsArray(j, i, numCols, numRows)
            '''

            Do something like

            '''
            # Temporal filtering
            Data = QueganTemporalSpeckleFiltering(Data,aTempWindowSize)

            #Clip the border
            Data = Data[:,iOffset:iOffset + numRowsDefault,jOffset:jOffset + numColsDefault]

            #We writte Quegan filtered data
            for band in range( NumBands ):
                band += 1
                bandP = GdalOutputFilePointerList[band - 1].GetRasterBand(1)
                bandP.SetNoDataValue(0)
                bandP.WriteArray(Data[band-1,:,:],jOriginal,iOriginal)

            BlockId = BlockId + 1

    # We close all file
    src_ds = None
    for band in range( NumBands ):
        GdalOutputFilePointerList[band] = None

def Int2dB(InputFile, Band, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += InputFile + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\"" + Band + "<=0.001 ? -30 : 10 * log10(" +Band +")\""

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

# OTB Create HVdB - HHdB
def DiffdB(aInputFileCopol, aInputFileCrosspol, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += aInputFileCopol + " "
	cmd += aInputFileCrosspol + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\" im2b1 - im1b1 \""

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

# OTB ratio
def RatioDualPol(aInputFileCopol, aInputFileCrosspol, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += aInputFileCopol + " "
	cmd += aInputFileCrosspol + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\" im2b1 / im1b1 \""

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GdalBuildVRT(aInputListFile, aOutputFile,  aSRCNoData, aVRTNoData):
	cmd = "gdalbuildvrt "
	cmd += " -separate "
	cmd += " -srcnodata " + str(aSRCNoData)
	cmd += " -vrtnodata " + str(aVRTNoData)
	cmd += " " + aOutputFile
	for File in aInputListFile:
		cmd += " " + File

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GenerateDualPolColorcompositiondB(aSubfolders, aOutputFolder):
	# Loop thru different S1 data (folder)
	for Folder in aSubfolders:
		# List all tif and tiff files
		AllTifFile = directory_liste_ext_files(Folder, 'tif')

		InDirName = os.path.split(Folder)[1]

		# Create Output subfolder
		OutFolder = os.path.join(aOutputFolder,InDirName)
		if not os.path.exists(OutFolder):
			os.makedirs(OutFolder)

		Dual = False
		if '1SDV' in Folder:
			CopolFile = [s for s in AllTifFile if "VV" in s][0]
			CrosspolFile = [s for s in AllTifFile if "VH" in s][0]
			Dual = True

		elif '1SDH' in Folder:
			CopolFile = [s for s in AllTifFile if "HH" in s][0]
			CrosspolFile = [s for s in AllTifFile if "HV" in s][0]
			Dual = True
		else:
			print 'Not dual pol data'

		if Dual:
			CopolFileName = os.path.basename(os.path.splitext(CopolFile)[0])
			CrosspolFileName = os.path.basename(os.path.splitext(CrosspolFile)[0])

			CopoldBFile = os.path.join(OutFolder,  CopolFileName.replace('_Clip_','_dB_Clip_') + '.tif')
			CrosspoldBFile = os.path.join(OutFolder,  CrosspolFileName.replace('_Clip_','_dB_Clip_') + '.tif')

			if '1SDV' in Folder:
				BaseNameFile = CopolFileName.replace('_VV_','_VHVV_')
				BaseNameFile = BaseNameFile.replace('_Clip_','_dB_Clip_')
				BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

				DiffFile = os.path.join(OutFolder,  BaseNameFile +'.tif')

				OutputColorCompVRTFile = os.path.join(OutFolder,  BaseNameFile.replace('_VHVV_','_VV_VH_VHVV_') +'.vrt')
			else:
				BaseNameFile = CopolFileName.replace('_HH_','_HVHH_')
				BaseNameFile = BaseNameFile.replace('_Clip_','_dB_Clip_')
				BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

				DiffFile = os.path.join(OutFolder,  BaseNameFile + '.tif')

				OutputColorCompVRTFile = os.path.join(OutFolder,  BaseNameFile.replace('_VHVV_','_HH_HV_HVHH_') +'.vrt')

			Int2dB(CopolFile, 'im1b1', CopoldBFile, Ram)
			Int2dB(CrosspolFile, 'im1b1', CrosspoldBFile, Ram)
			DiffdB(CopoldBFile, CrosspoldBFile, DiffFile, Ram)



			# VRT color composition
			GdalBuildVRT([CopoldBFile, CrosspoldBFile, DiffFile], OutputColorCompVRTFile,  0, 0)


def GenerateDualPolColorcompositionInt(aSubfolders, aOutputFolder):
	# Loop thru different S1 data (folder)
	for Folder in aSubfolders[1:]:
		# List all tif and tiff files
		AllTifFile = directory_liste_ext_files(Folder, 'tif')

		InDirName = os.path.split(Folder)[1]

		# Create Output subfolder
		OutFolder = os.path.join(aOutputFolder,InDirName)
		if not os.path.exists(OutFolder):
			os.makedirs(OutFolder)

		Dual = False
		if '1SDV' in Folder:
			CopolFile = [s for s in AllTifFile if "VV" in s][0]
			CrosspolFile = [s for s in AllTifFile if "VH" in s][0]
			Dual = True

		elif '1SDH' in Folder:
			CopolFile = [s for s in AllTifFile if "HH" in s][0]
			CrosspolFile = [s for s in AllTifFile if "HV" in s][0]
			Dual = True
		else:
			print 'Not dual pol data'

		if Dual:
			CopolFileName = os.path.basename(os.path.splitext(CopolFile)[0])
			CrosspolFileName = os.path.basename(os.path.splitext(CrosspolFile)[0])

			if '1SDV' in Folder:
				BaseNameFile = CopolFileName.replace('_VV_','_VHVV_')
				BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

				Ratio = os.path.join(OutFolder,  BaseNameFile + '.tif')

				OutputColorCompVRTFile = os.path.join(OutFolder,  BaseNameFile.replace('_VHVV_','_VV_VH_VHVV_') +'.vrt')
			else:
				BaseNameFile = CopolFileName.replace('_HH_','_HVHH_')
				BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])
				Ratio = os.path.join(OutFolder,  BaseNameFile +'.tif')

				OutputColorCompVRTFile = os.path.join(OutFolder,  BaseNameFile.replace('_VHVV_','_HH_HV_HVHH_') + '.vrt')

			RatioDualPol(CopolFile, CrosspolFile, Ratio, Ram)

			# VRT color composition
			GdalBuildVRT([CopolFile, CrosspolFile, Ratio], OutputColorCompVRTFile,  0, 0)

'''
Program

1 - List all files
2 - split one list for vh one fo hh one for vv
3 - create the output subdirectories
4 - do the filtering (if lee, in tmp)

'''

# create tmp dir
TmpDir = os.path.join(OutputFolder, "tmp")
if not os.path.exists(TmpDir):
	os.makedirs(TmpDir)

# Create Temp Filtering Tmp Dir
TmpDirTempFilt = os.path.join(TmpDir, "TempFilt")
if not os.path.exists(TmpDirTempFilt):
	os.makedirs(TmpDirTempFilt)



# List all SENTINEL-1 sub directories
SubFolders = get_immediate_subdirectories(DataFolder)

# If Lee
if Apply_Lee_Pre_Filtering:
	# Loop thru different S1 data (folder)
	for Folder in SubFolders:
		# List all tif and tiff files
		AllTifFile = directory_liste_ext_files(Folder, 'tif')

		InDirName = os.path.split(Folder)[1]

		# Create Output subfolder
		OutFolder = os.path.join(TmpDir,InDirName)
		if not os.path.exists(OutFolder):
			os.makedirs(OutFolder)

		for file in AllTifFile:
			FileName = os.path.basename(os.path.splitext(file)[0])
			OutputFile = os.path.join(OutFolder,  FileName + '_SpkLee_W' + str(Window_Size) + '_NL' + str(Number_Of_Look) +'.tif')

			ApplyLeeFiltering(file,OutputFile, Radius, Number_Of_Look, Ram)

	# Update Input dir to be Tmp folder
	DataFolder = TmpDir



AllTifFile = directory_liste_ext_files(DataFolder, 'tif')
AllCopolFile = [file for file in AllTifFile if ('VV' in file) or  ('HH' in file)]
AllCrosspolFile = [file for file in AllTifFile if ('HV' in file) or  ('VH' in file)]

# Create Output list data
AllOutCopolFile = []
AllOutCrosspolFile = []

if not Output_in_dB:
	TmpDirTempFilt = OutputFolder

for CopolFile in AllCopolFile:
	# DirName of currentfile
	DirFile =  os.path.dirname(CopolFile)
	DirName = os.path.split(DirFile)[1]

	# Create Output subfolder
	OutFolder = os.path.join(TmpDirTempFilt,DirName)
	if not os.path.exists(OutFolder):
		os.makedirs(OutFolder)

	FileName = os.path.basename(os.path.splitext(CopolFile)[0])
	TempFilterFileName = os.path.join(OutFolder,FileName + '_TempFilt_W' + str(Window_Temp_Filtering) + '.tif')
	AllOutCopolFile.append(TempFilterFileName)

for CrosspolFile in AllCrosspolFile:
	# DirName of currentfile
	DirFile =  os.path.dirname(CrosspolFile)
	DirName = os.path.split(DirFile)[1]

	# Create Output subfolder
	OutFolder = os.path.join(TmpDirTempFilt,DirName)
	if not os.path.exists(OutFolder):
		os.makedirs(OutFolder)

	FileName = os.path.basename(os.path.splitext(CrosspolFile)[0])
	TempFilterFileName = os.path.join(OutFolder,FileName + '_TempFilt_W' + str(Window_Temp_Filtering) + '.tif')
	AllOutCrosspolFile.append(TempFilterFileName)

NumDate = len(AllCopolFile)
BlockSize = int(np.sqrt(float(Ram) * np.power(1024,2) /(4. * 2. *(2. * float(NumDate + 1.)))))

# Apply filtering to Copol
TileRasterProcessing(AllCopolFile, AllOutCopolFile, BlockSize,Window_Temp_Filtering)

# Apply filtering to crosspol
TileRasterProcessing(AllCrosspolFile, AllOutCrosspolFile, BlockSize,Window_Temp_Filtering)

# Generate ratio intensity or convert in dB and creat vrt for color composition
# List all folders in Temp Filtering forlder
# Get full path of Temp filtering folder
SubFolders = get_immediate_subdirectories(TmpDirTempFilt)

if Output_in_dB:
	GenerateDualPolColorcompositiondB(SubFolders, OutputFolder)
else:
	GenerateDualPolColorcompositionInt(SubFolders, OutputFolder)

shutil.rmtree(TmpDir)
