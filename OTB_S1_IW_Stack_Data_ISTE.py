##ISTE: Sentinel-1 IW Batch Processing (miscellaneous)=group
##Clip over largest Area=name
##Input_Data_Folder=folder
##Output_Pixel_Size=number 10
##Output_EPSG=crs
##Output_Data_Folder=folder
##Ram=number 256

'''
Date:  Juillet 2017
Auteurs:
Cedric Lardeux: clardeux_at_gmail.com cedric.lardeux_at_onfinternational.com
Pierre-Louis: Frison pierre-louis.frison_at_u-pem.fr
'''

import sys, shutil
import glob, os, zipfile, os.path
import string
import subprocess
from datetime import datetime, date, time
from osgeo import ogr
from osgeo import gdal

'''
INPUT
'''
DataFolder = Input_Data_Folder
OutputFolder = Output_Data_Folder

def DelDirAndItsFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
	os.removedirs(aInputDir)

def DelDirFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [os.path.join(a_dir, name) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
	list_file = []
	for root, dirnames, filenames in os.walk(directory):
		for filename in filenames:
			if filename.endswith((filt_ext)):
				list_file.append(os.path.join(root, filename))

	list_file.sort()
	return list_file

def Polygonize(aInputRaster, aOutputVector):
	# Multilook
	if 'nt' in os.name:
		cmd = "gdal_polygonize.bat   "
	else:
		cmd = "gdal_polygonize.py   "

	cmd += aInputRaster + " "
	cmd += " -mask " + aInputRaster
	cmd += " -b 1 -f \"ESRI Shapefile\" "
	cmd += aOutputVector

	# progress.setInfo('\nPolygonize ' + cmd)
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GdalClipRasterWithVector(aInputRaster, aInputVector, aOutputFile, aOutputResolution, aSRCNoData, aOutEPSG):
	cmd = "gdalwarp "
	cmd += " -tr " + str(aOutputResolution) + ' ' + str(aOutputResolution) + ' '
	cmd += " -t_srs " + str(aOutEPSG)
	cmd += " -q -multi -crop_to_cutline -cutline "
	cmd += aInputVector
	cmd += " -dstnodata " + str(aSRCNoData)
	cmd += " -of GTiff " + aInputRaster + ' ' + aOutputFile
	
	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]


def GdalVrtStacking(aListFile, aOutputRaster, aNodataVal, aSeparate):
    cmd = 'gdalbuildvrt '
    if aSeparate: cmd += '-separate '
    cmd += ' -resolution highest '
    cmd += ' -srcnodata ' + str(aNodataVal) + ' -vrtnodata '+ str(aNodataVal) + ' '
    cmd += '-input_file_list ' + aListFile + ' -overwrite '
    cmd += aOutputRaster

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

def GdalVrtSplitBands(aInputRaster):
    InputRasterName = os.path.splitext(os.path.basename(aInputRaster))[0]
    InputRasterFolder = os.path.dirname(aInputRaster)

    print aInputRaster

    ds=gdal.Open(aInputRaster)
    bands = ds.RasterCount
    ds = None

    OutListFiles = []
    for band in range(bands):
        OutFile = os.path.join(InputRasterFolder,InputRasterName + '_b'+str(band+1)+'.vrt')
        OutListFiles.append(OutFile)

        cmd = 'gdalbuildvrt '
        cmd += '-srcnodata 0 -vrtnodata 0 '
        cmd += " -b " + str(band + 1) + ' '
        cmd += OutFile + ' '
        cmd += aInputRaster + ' '

        # progress.setInfo(cmd)
        # print cmd

        p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
        ret= p1.communicate()[1]

    return OutListFiles

def OTBProduct(aInputRasterList, aOutputRaster, aRam):
	NumBand = len(aInputRasterList)
	cmd = "otbcli_BandMath -il "
	for File in aInputRasterList:
		cmd += " " + File
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += aOutputRaster
	cmd += " -exp "
	cmd += "\"("
	for i in range(NumBand):
		cmd += " " + "im" + str(i+1) + 'b1*'
	cmd = cmd[:-1]
	cmd += " )>0\""

	# progress.setInfo(cmd)
	print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def CreateSmallestOverlap(aInputRasterList, aOutputShape, aRam):
	DirShape = os.path.dirname(aOutputShape)
	TmpDir = os.path.join(DirShape,datetime.now().strftime("%Y_%m_%d_%H_%M_%S"))

	if not os.path.exists(TmpDir):
		os.makedirs(TmpDir)
	
	# Create multiband vrt to do the stack (use resolution and epsg)
	OutputVRTStack = os.path.join(TmpDir, 'Stack.vrt')
	# Create txt list file
	OutputListFiles = os.path.join(TmpDir, 'files.txt')
	MyOutputTxt = open(OutputListFiles, "w")

	for file in aInputRasterList:
		 MyOutputTxt.write(file + '\n')
	MyOutputTxt.close()

	GdalVrtStacking(OutputListFiles, OutputVRTStack, str(0), True)

	# Split the band into vrt to generate mask
	ListBands = GdalVrtSplitBands(OutputVRTStack)

	# Multiply all vrt to have the raster mask overlap
	OutputRasterMask = os.path.join(TmpDir, 'rastermask.tif')
	OTBProduct(ListBands, OutputRasterMask, aRam)

	# Polygonise the results
	Polygonize(OutputRasterMask, aOutputShape)

	# Del TmpOrtho folder
	shutil.rmtree(TmpDir)


'''
Program
'''

# List all SENTINEL-1 sub directories
SubFolders = get_immediate_subdirectories(DataFolder)


# List all tif file and generate shape footprint
AllTifFile = directory_liste_ext_files(DataFolder, 'tif')


CommonShapeArea = os.path.join(OutputFolder,'CommonFootprintArea.shp')
CreateSmallestOverlap(AllTifFile, CommonShapeArea, Ram)

# Clip the data
# 
# Loop thru different S1 data (folder) to create common shape file footprint
for Folder in SubFolders:
	# List all tif and tiff files
	AllTifFile = directory_liste_ext_files(Folder, 'tif')

	InDirName = os.path.split(Folder)[1]

	# Create Output subfolder
	OutFolder = os.path.join(OutputFolder,InDirName)
	if not os.path.exists(OutFolder):
		os.makedirs(OutFolder)

	for file in AllTifFile:
		FileName = os.path.basename(os.path.splitext(file)[0])
		OutputFile = os.path.join(OutFolder,  FileName + '_Clip' +'.tif')

		GdalClipRasterWithVector(file,CommonShapeArea , OutputFile, Output_Resolution,  0, Output_EPSG)

# Del Temporay Dir
# DelDirAndItsFiles(TmpDir)
