##ISTE: Sentinel-1 IW Batch Processing (miscellaneous)=group
##Clip ortho data over Polygon=name
##Input_Data_Folder=folder
##Input_Polygon_File=vector
##Output_EPSG=crs
##Output_Resolution=number 10
##Output_Data_Folder=folder
##Ram=number 256

'''
Date:  Juillet 2017
Auteurs:
Cedric Lardeux: clardeux_at_gmail.com cedric.lardeux_atonfinternational.com
Pierre-Louis Frison: pierre-louis.frison_atu-pem.fr
'''
import sys, shutil
import glob, os, zipfile, os.path
import string
import subprocess
from datetime import datetime, date, time
from osgeo import ogr
from scipy.ndimage.filters import uniform_filter
from osgeo import gdal
import numpy as np

# This script is dedicated to orthorectified Sentinel-1 data in GRD IW mode and dual pol (VV, VH or HH, VH)
# It's based on OTB 5.6

'''
INPUT
'''

DataFolder = Input_Data_Folder
OutputFolder = Output_Data_Folder

def DelDirAndItsFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
	os.removedirs(aInputDir)

def DelDirFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [os.path.join(a_dir, name) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
	list_file = []
	for root, dirnames, filenames in os.walk(directory):
		for filename in filenames:
			if filename.endswith((filt_ext)):
				list_file.append(os.path.join(root, filename))

	list_file.sort()
	return list_file

'''
This function unzip one given file in a given directory
'''
def unzip(source_filename, dest_dir):
    with zipfile.ZipFile(source_filename) as zf:
        for member in zf.infolist():
            # Path traversal defense copied from
            # http://hg.python.org/cpython/file/tip/Lib/http/server.py#l789
            words = member.filename.split('/')
            path = dest_dir
            for word in words[:-1]:
                while True:
                    drive, word = os.path.splitdrive(word)
                    head, word = os.path.split(word)
                    if not drive:
                        break
                if word in (os.curdir, os.pardir, ''):
                    continue
                path = os.path.join(path, word)
            zf.extract(member, path)



def ReprojVector(aInputVector, aOutputVector, aEPSG):
	cmd = "ogr2ogr "
	cmd += " -f \"ESRI Shapefile\"  " + aOutputVector+ " "
	cmd +=  aInputVector  + " "
	cmd += " -t_srs EPSG:" + str(aEPSG)

	# print cmd
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def Rasterize(aInputVector, aOutputRaster, aOutPixelSize):
	# Multilook
	cmd = "gdal_rasterize "
	cmd += " -burn 1 "
	cmd += " -of GTiff "
	cmd += " -a_nodata 0 "
	cmd += " -a_srs EPSG:3857 "
	cmd += " -tr " + str(aOutPixelSize) + " "+ str(aOutPixelSize)+ " "
	cmd += aInputVector + " "
	cmd += aOutputRaster

	# print cmd
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]



def GdalClipRasterWithVector(aInputRaster, aInputVector, aOutputFile, aOutputResolution, aSRCNoData, aOutEPSG):
	cmd = "gdalwarp "
	cmd += " -tr " + str(aOutputResolution) + ' ' + str(aOutputResolution) + ' '
	cmd += " -t_srs " + str(aOutEPSG)
	cmd += " -r average -q -multi -crop_to_cutline -cutline "
	cmd += aInputVector
	cmd += " -dstnodata " + str(aSRCNoData)
	cmd += " -of GTiff " + aInputRaster + ' ' + aOutputFile

	# progress.setInfo(cmd)
	# print cmd

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]



def GdalVrtStacking(aListFile, aOutputRaster, aNodataVal, aSeparate):
    cmd = 'gdalbuildvrt '
    if aSeparate: cmd += '-separate '
    cmd += ' -resolution highest '
    cmd += ' -srcnodata ' + str(aNodataVal) + ' -vrtnodata '+ str(aNodataVal) + ' '
    cmd += '-input_file_list ' + aListFile + ' -overwrite '
    cmd += aOutputRaster

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]


def GdalBuildVRT(aInputListFile, aOutputFile,  aSRCNoData, aVRTNoData):
	cmd = "gdalbuildvrt "
	cmd += " -separate "
	cmd += " -srcnodata " + str(aSRCNoData)
	cmd += " -vrtnodata " + str(aVRTNoData)
	cmd += " " + aOutputFile
	for File in aInputListFile:
		cmd += " " + File
	
	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GenerateDualPolColorcompositiondB(aSubfolders, aOutputFolder):
	# Loop thru different S1 data (folder)
	for Folder in aSubfolders:
		# List all tif and tiff files
		AllTifFile = directory_liste_ext_files(Folder, 'tif')

		InDirName = os.path.split(Folder)[1]

		# Create Output subfolder
		OutFolder = os.path.join(aOutputFolder,InDirName)
		if not os.path.exists(OutFolder):
			os.makedirs(OutFolder)

		Dual = False
		if '1SDV' in Folder:
			CopolFile = [s for s in AllTifFile if "VV" in s][0]
			CrosspolFile = [s for s in AllTifFile if "VH" in s][0]
			Dual = True

		elif '1SDH' in Folder:
			CopolFile = [s for s in AllTifFile if "HH" in s][0]
			CrosspolFile = [s for s in AllTifFile if "HV" in s][0]
			Dual = True
		else:
			print 'Not dual pol data'

		if Dual:
			CopolFileName = os.path.basename(os.path.splitext(CopolFile)[0])
			CrosspolFileName = os.path.basename(os.path.splitext(CrosspolFile)[0])

			CopoldBFile = os.path.join(OutFolder,  CopolFileName + '_dB' +'.tif')
			CrosspoldBFile = os.path.join(OutFolder,  CrosspolFileName + '_dB' +'.tif')

			if '1SDV' in Folder:
				BaseNameFile = CopolFileName.replace('VV','')
				BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

				DiffFile = os.path.join(OutFolder,  BaseNameFile + '_VHdB-VVdB' +'.tif')

				OutputColorCompVRTFile = os.path.join(OutFolder,  BaseNameFile + 'VVdB_VHdB_HVdB-VVdB' +'.vrt')
			else:
				BaseNameFile = CopolFileName.replace('HH','')
				BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

				DiffFile = os.path.join(OutFolder,  BaseNameFile + 'HVdB-HHdB' +'.tif')

				OutputColorCompVRTFile = os.path.join(OutFolder,  BaseNameFile + 'HHdB_HVdB_HVdB-HHdB' +'.vrt')
			
			Int2dB(CopolFile, 'im1b1', CopoldBFile, Ram)
			Int2dB(CrosspolFile, 'im1b1', CrosspoldBFile, Ram)
			DiffdB(CopoldBFile, CrosspoldBFile, DiffFile, Ram)



			# VRT color composition
			GdalBuildVRT([CopoldBFile, CrosspoldBFile, DiffFile], OutputColorCompVRTFile,  0, 0)

def TileRasterProcessingAverage(aInputRasterListPath, aOutputRaster, aBlockSize):
    '''
    TODO: Add sliding window parameter

    This method show how to process huge raster
    using easy tilling based on block size
    and if necessary taking into account window size
    when necessary in processing like filtering for example
    aInputRasterPath string --> Contain input raster path to process
    aOutputRasterPath string --> Contain output raster path to process
    aBlockSize integer --> Contain the size of square block to Tile
    aWindowSize integer --> Contain windows size for sliding window (0 size by default)
    '''
    aWindowSize = 0

    # we put X and Y block size based on aBlockSize
    xBSize = aBlockSize
    yBSize = aBlockSize


    NumBands = len(aInputRasterListPath)

    # We open one raster to get rows and cols
    src_ds = gdal.Open( aInputRasterListPath[0] )
    if src_ds is None:
        print 'Could not open ' + fn
        sys.exit(1)

    # We get number of row and cols
    rows = src_ds.RasterYSize
    cols = src_ds.RasterXSize

    print 'rows, cols', rows, cols

    # We force Float32 to read data
    BandType = gdal.GDT_Float32

    # We get Projection from input raster
    InputGeoTransform = src_ds.GetGeoTransform()
    InputProjection = src_ds.GetProjection()

    src_ds = None

    # Open input files
    GdalFilePointerList = []
    GdalOutputFilePointerList = []
    for i in range(NumBands):
        GdalFilePointerList.append(gdal.Open( aInputRasterListPath[i] ))

    # Output file
    format = "GTiff"
    driver = gdal.GetDriverByName( format )
    dst_raster = driver.Create(aOutputRaster, cols, rows, 1, BandType )
    dst_raster.SetGeoTransform(InputGeoTransform)
    dst_raster.SetProjection(InputProjection)

    # print 'Rows, Cols: ',rows, cols
    BlockId = 0
    for i in range(0, rows, yBSize):
        if i + yBSize < rows:
            numRows = yBSize
        else:
            numRows = rows - i
        # Backup i and numRows
        numRowsDefault = numRows
        iOriginal = i
        for j in range(0, cols, xBSize):
            i = iOriginal
            numRows = numRowsDefault
            print '\n Block numero : ', BlockId
            if j + xBSize < cols:
                numCols = xBSize
            else:
                numCols = cols - j
            numColsDefault = numCols

            # print 'Indice i,j original : ', i, j
            # print 'numRows numCols :',numRows, numCols
            # Test for applying sliding window buffer
            # Backup j
            jOriginal = j

            iOffset = 0
            jOffset = 0
            numColsOffset = 0
            numRowsOffset = 0
            if i - aWindowSize >= 0:
                i = i - aWindowSize
                iOffset = aWindowSize
            if j - aWindowSize >= 0:
                j = j - aWindowSize
                jOffset = aWindowSize

            if jOriginal + numCols + aWindowSize <= cols:
                numCols = numCols + aWindowSize
                numColsOffset = aWindowSize
            numCols = numCols + jOffset

            if iOriginal + numRows + aWindowSize <= rows:
                numRows = numRows + aWindowSize
                numRowsOffset = aWindowSize
            numRows = numRows + iOffset

            # print 'Read as array j, i, numCols, numRows', j, i, numCols, numRows
            Data = np.zeros(shape=(NumBands, numRows, numCols ),dtype=np.float)

            #We get file values
            for it in range(len(GdalFilePointerList)):
                GetBand = GdalFilePointerList[it].GetRasterBand(1)
                Data[it, :,:] = GetBand.ReadAsArray(j, i, numCols, numRows)
            '''

            Do something like

            '''
            # Temporal average
            Mean = np.mean(Data, axis=0)

            print 'np.shape(Data)',np.shape(Data)
            print 'np.shape(Mean)',np.shape(Mean)

            #Clip the border
            Mean = Mean[iOffset:iOffset + numRowsDefault,jOffset:jOffset + numColsDefault]

            print 'np.shape(Mean)',np.shape(Mean)

            #We writte Quegan filtered data
            band = dst_raster.GetRasterBand(1)
            band.SetNoDataValue(0)
            band.WriteArray(Mean,jOriginal,iOriginal)

            BlockId = BlockId + 1

    # We close all file
    src_ds = None
    dst_raster = None

def Int2dB(InputFile, Band, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += InputFile + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\"" + Band + "==0 ? 0 : 10 * log10(" +Band +")\""

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def DiffdB(aInputFileCopol, aInputFileCrosspol, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += aInputFileCopol + " "
	cmd += aInputFileCrosspol + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\" im2b1 - im1b1 \""

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def RatioDualPol(aInputFileCopol, aInputFileCrosspol, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += aInputFileCopol + " "
	cmd += aInputFileCrosspol + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\" im2b1 / im1b1 \""

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def getDayFromS1File(aFileName):
	aFileName = aFileName.split("/")[-1]
#	return aFileName.split("_")[4][0:8]
	pos=aFileName.find('_2015') or aFileName.find('_2016') or aFileName.find('_2017') or aFileName.find('_2018') or aFileName.find('_2019') or aFileName.find('_2020')
	return aFileName[pos+1:pos+9]

def GenerateTemporalAverage(aInputFolder):
	AllTifFile = directory_liste_ext_files(aInputFolder, 'tif')
	AllTifFile = [ file for file in AllTifFile if 'Quegan' not in file]
	AllCopolFile = [file for file in AllTifFile if ('VV' in file) or  ('HH' in file)]
	AllCrosspolFile = [file for file in AllTifFile if ('HV' in file) or  ('VH' in file)]


	# We get acquisition date
	ListDates = [getDayFromS1File(RastPath) for RastPath in AllCopolFile ]
	UniqueOutputDates = list(set(ListDates))
	UniqueOutputDates.sort()

	NumDate = len(AllCopolFile)

	BlockSize = int(np.sqrt(Ram * np.power(1024,2) /(4. * 2. *(2. * NumDate + 1.))))

	print Ram, NumDate, BlockSize
	OutputDateSuffix = UniqueOutputDates[0] + '_' + UniqueOutputDates[-1] + '.tif'

	# Apply average to Copol
	OutputCopol = os.path.join(aInputFolder, 'TempAverage_VV_' + OutputDateSuffix)
	TileRasterProcessingAverage(AllCopolFile, OutputCopol, BlockSize)


	# Apply filtering to crosspol
	OutputCrosspol = os.path.join(aInputFolder, 'TempAverage_VH_' + OutputDateSuffix)
	TileRasterProcessingAverage(AllCrosspolFile, OutputCrosspol, BlockSize)

	# Generate ratio intensity or convert in dB and creat vrt for color composition
	# List all folders in Time Filtering forlder
	# Get full path of time filtering folder
	GenerateDualPolColorcompositiondB(OutputCopol, OutputCrosspol)

'''
Program
'''
# Stack Process
# List all SENTINEL-1 sub directories
SubFolders = get_immediate_subdirectories(DataFolder)

# Clip the data
#
# Loop thru different S1 data (folder) to create common shape file footprint
for Folder in SubFolders:
	# List all tif and tiff files
	AllTifFile = directory_liste_ext_files(Folder, 'tif')

	InDirName = os.path.split(Folder)[1]

	# Create Output subfolder
	OutFolder = os.path.join(OutputFolder,InDirName)
	if not os.path.exists(OutFolder):
		os.makedirs(OutFolder)

	for file in AllTifFile:
		FileName = os.path.basename(os.path.splitext(file)[0])
		OutputFile = os.path.join(OutFolder,  FileName + '_Clip' +'.tif')

		GdalClipRasterWithVector(file,Input_Polygon_File , OutputFile, Output_Resolution,  0, Output_EPSG)

GenerateTemporalAverage(OutputFolder)
