##ISTE: Sentinel-1 IW Batch Processing (miscellaneous)=group
##GRD Products Cal. and Orthorect.=name
##Input_Data_Folder=folder
##DEM_Folder=folder
##Calibration_Type=string Sigma0
##Output_Data_Folder=folder
##Ram=number 256

'''
Date:  Juillet 2017
Auteurs:
Cedric Lardeux: clardeux_at_gmail.com cedric.lardeux_at_onfinternational.com
Pierre-Louis Frison: pierre-louis.frison_at_u-pem.fr
'''

import sys
import glob, os, zipfile, os.path
import string
import subprocess
from datetime import datetime, date, time

# This script is dedicated to orthorectified Sentinel-1 data in GRD IW mode and dual pol (VV, VH or HH, VH)
# It's based on OTB 5.6

# Input_Data_Folder='/media/onfi/CL/livre_QGIS/Input'
# Clip_With_Vector= True
# Input_Vector_File = '/media/onfi/CL/livre_QGIS/Input/roi.shp'
# Calibration_Type='Sigma0'
# Output_Data_Folder='/media/onfi/CL/livre_QGIS/output'
Download_SRTM=False
# DEM_Folder='/media/onfi/CL/livre_QGIS/DEM/USGS'
# Ram=1000


'''
INPUT
'''
Noise = 'false'
if Calibration_Type == 'Sigma0':
	Calibration_Type = 'sigma'
	Calib_Name='Sig0'
elif Calibration_Type == 'Gamma0':
	Calibration_Type = 'gamma'
	Calib_Name='Gam0'
elif Calibration_Type == 'Beta0':
	Calibration_Type = 'beta'
	Calib_Name='Bet0'
else:
	Calibration_Type='sigma'
	Calib_Name='Sig0'

DemDir = DEM_Folder

DataFolder = Input_Data_Folder
OutputFolder = Output_Data_Folder

def DelDirAndItsFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
	os.removedirs(aInputDir)

def DelDirFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [os.path.join(a_dir, name) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
	list_file = []
	for root, dirnames, filenames in os.walk(directory):
		for filename in filenames:
			if filename.endswith((filt_ext)):
				list_file.append(os.path.join(root, filename))

	list_file.sort()
	return list_file

'''
This function unzip one given file in a given directory
'''
def unzip(source_filename, dest_dir):
    with zipfile.ZipFile(source_filename) as zf:
        for member in zf.infolist():
            # Path traversal defense copied from
            # http://hg.python.org/cpython/file/tip/Lib/http/server.py#l789
            words = member.filename.split('/')
            path = dest_dir
            for word in words[:-1]:
                while True:
                    drive, word = os.path.splitdrive(word)
                    head, word = os.path.split(word)
                    if not drive:
                        break
                if word in (os.curdir, os.pardir, ''):
                    continue
                path = os.path.join(path, word)
            zf.extract(member, path)

'''
This function calibrate SAR file
aNoise 	--> to remove (or not) noise 	- True or False
aCalibration_Type 	--> to chose aCalibration_Type 		- sigma/gamma/beta/dn
'''
def OTBSARCalibration(aInputFile, aOutputFile, aNoise, aCalibration_Type, aRam):
	cmd = "otbcli_SARCalibration -in "
	cmd += aInputFile + " "
	cmd += " -ram " + str(aRam)
	cmd += " -out "
	cmd += aOutputFile
	cmd += " -noise " + aNoise
	cmd += " -lut " + aCalibration_Type

	progress.setInfo(cmd)
	# print cmd

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

'''
This function do orthorectification of any optical/radar file
aDEMDir 	--> directory that contain DEM

opt.gridspacing is not in parameter function but need to be near 4 to 10 times of output pixel size
For example, with 10m output pixel size, you can chose bettween 40 to 100.
More less is the value more accurate is the results but more long
In addition this value have to be linked to DEM pixel size
'''
def OTBOrthorectification(aInputFile, aOutputFile, aDEMDir , aRam):
	cmd = "otbcli_OrthoRectification -io.in "
	cmd += aInputFile + " "
	cmd += " -opt.ram " + str(aRam)
	cmd += " -io.out "
	cmd += aOutputFile
	cmd += " -elev.dem " + aDEMDir
	cmd += " -opt.gridspacing 40 "

	progress.setInfo(cmd)
	# print cmd

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def OTBExtractRoi(aInputFile, aInputRefFile, aOutputFile, aRam):
	cmd = "otbcli_ExtractROI -in "
	cmd += aInputFile + " "
	cmd += " -ram " + str(aRam)
	cmd += " -out "
	cmd += aOutputFile
	cmd += " -mode fit "
	cmd += " -mode.fit.ref " + aInputRefFile

	# progress.setInfo(cmd)
	# print cmd

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def ReprojVector(aInputVector, aOutputVector, aEPSG):
	cmd = "ogr2ogr "
	cmd += " -f \"ESRI Shapefile\"  " + aOutputVector+ " "
	cmd +=  aInputVector  + " "
	cmd += " -t_srs EPSG:" + str(aEPSG)

	# print cmd
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def Rasterize(aInputVector, aOutputRaster, aOutPixelSize):
	# Multilook
	cmd = "gdal_rasterize "
	cmd += " -burn 1 "
	cmd += " -of GTiff "
	cmd += " -a_nodata 0 "
	cmd += " -a_srs EPSG:3857 "
	cmd += " -tr " + str(aOutPixelSize) + " "+ str(aOutPixelSize)+ " "
	cmd += aInputVector + " "
	cmd += aOutputRaster

	# print cmd
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

# OTB 
def OTBDownloadSRTMTile(aInputFile, aDEMDir):
	cmd = "otbcli_DownloadSRTMTiles -il "
	cmd += aInputFile + " "
	cmd += " -mode.download.outdir " + aDEMDir

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GRD2Calib_Ortho(aInputFile,aOutputFile, aCalibration_Type, aNoise, aRam):
	TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

	# Output Folder Name
	OutputFolderName = os.path.dirname(aOutputFile)

	# Output Intermediate Calibrated file
	OutputCalibratedFile = os.path.join(TmpDir, TimeNow + "_Calib.tif")

	# Calibration
	OTBSARCalibration(aInputFile, OutputCalibratedFile, aNoise, aCalibration_Type, aRam)

	# Output DEM Dir
	if Download_SRTM:
		OutputDEMDir = os.path.join(DemDir, 'Download' )
	else:
		OutputDEMDir = DemDir
	
	# Download SRTM DEM
	if not os.path.exists(OutputDEMDir):
	    os.makedirs(OutputDEMDir)

	if Download_SRTM:
		OTBDownloadSRTMTile(OutputCalibratedFile, OutputDEMDir)
		ZipDemFiles = directory_liste_ext_files(OutputDEMDir, '.zip')

		for zipFile in ZipDemFiles:
			unzip(zipFile, OutputDEMDir)

	# Orthorectification
	OTBOrthorectification(OutputCalibratedFile, aOutputFile, OutputDEMDir, aRam)

'''
Program
'''
# create tmp dir
TmpDir = os.path.join(OutputFolder, "tmp")
if not os.path.exists(TmpDir):
	os.makedirs(TmpDir)

# List all SENTINEL-1 sub directories
SubFolders = get_immediate_subdirectories(DataFolder)

S1DataFolders = [ Folder for Folder in SubFolders if '.SAFE' in Folder ]

# Loop thru different S1 data (folder)
for Folder in S1DataFolders:
	TiffFiles = directory_liste_ext_files(Folder, 'tiff')

	SafeName = os.path.split(Folder)[-1]
	FolderName = SafeName.split('.')[0]

	WorkingFolder = os.path.join(OutputFolder,FolderName)
	if not os.path.exists(WorkingFolder):
		os.makedirs(WorkingFolder)

	OutputFilesOrtho = []
	for TiffFile in TiffFiles:
		if 'grd-vh' in TiffFile:
			SuffixOrtho = '_VH_'

		if 'grd-hv' in TiffFile:
			SuffixOrtho = '_HV_'
			
		if 'grd-vv' in TiffFile:
			SuffixOrtho = '_VV_'

		if 'grd-hh' in TiffFile:
			SuffixOrtho = '_HH_'

		SuffixOrtho += Calib_Name+'_Ortho.tif'

		OutputFile = os.path.join(OutputFolder,FolderName,FolderName + SuffixOrtho)

		# Do GRD conversion to Ortho (including calibration)
		GRD2Calib_Ortho(TiffFile,OutputFile, Calibration_Type, Noise, Ram)

		# Del Temporay files
		DelDirFiles(TmpDir)

# Del Temporay Dir
DelDirAndItsFiles(TmpDir)
