Ce Git contient tous les codes decrits dans le chapitre 6 "Vegetation cartography from Sentinel-1 Radar images" du livre "QGIS ad Applications in Agriculture and Forest":
<https://www.wiley.com/en-us/QGIS+and+Applications+in+Agriculture+and+Forest-p-9781786301888>

Auteurs:
   Pierre-Louis Frison (LaSTIG - UPEM/IGN) & Cedric Lardeux (ONF International)
- <pierre-louis.frison@u-pem.fr>
- <clardeux@gmail.com>
