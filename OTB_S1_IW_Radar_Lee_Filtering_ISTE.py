##ISTE: Sentinel-1 IW Batch Processing (miscellaneous)=group
##GRD Products Lee Speckle Filtering=name
##Input_Data_Folder=folder
##Window_Size=number 5
##Looks_Number=number 5
##Output_Data_Folder=folder
##Ram=number 256

'''
Date:  Juillet 2017
Auteurs:
Cedric Lardeux: clardeux_at_gmail.com cedric.lardeux_at_onfinternational.com
Pierre-Louis Frison: pierre-louis.frison_at_u-pem.fr
'''

import sys
import glob, os, zipfile, os.path
import string
import subprocess
from datetime import datetime, date, time

# This script is dedicated to orthorectified Sentinel-1 data in GRD IW mode and dual pol (VV, VH or HH, VH)
# It's based on OTB 5.6

# Input_Data_Folder='/media/onfi/CL/livre_QGIS/output'
# Radius=5
# Number_Of_Look=5
# Ram=1000


'''
INPUT
'''
DataFolder = Input_Data_Folder
OutputFolder = Output_Data_Folder
Radius = int(Window_Size / 2.)


'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [os.path.join(a_dir, name) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
	list_file = []
	for root, dirnames, filenames in os.walk(directory):
		for filename in filenames:
			if filename.endswith((filt_ext)):
				list_file.append(os.path.join(root, filename))

	list_file.sort()
	return list_file

def ApplyLeeFiltering(aInputFile,aOutputFile, aLeeRadius, aENL, aRam):
	cmd = "otbcli_Despeckle"
	cmd += " -in " + aInputFile
	cmd += " -filter lee"
	cmd += " -filter.lee.rad " + str(aLeeRadius)
	cmd += " -filter.lee.nblooks " + str(aENL)
	cmd += " -ram " + str(aRam)
	cmd += " -out " + aOutputFile

	# progress.setInfo(cmd)

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]
'''
Program
'''
# List all SENTINEL-1 sub directories
SubFolders = get_immediate_subdirectories(DataFolder)

# Loop thru different S1 data (folder)
for Folder in SubFolders:
	# List all tif and tiff files
	AllTifFile = directory_liste_ext_files(Folder, 'tif')

	InDirName = os.path.split(Folder)[1]

	# Create Output subfolder
	OutFolder = os.path.join(OutputFolder,InDirName)
	if not os.path.exists(OutFolder):
		os.makedirs(OutFolder)

	for file in AllTifFile:
		FileName = os.path.basename(os.path.splitext(file)[0])
		OutputFile = os.path.join(OutFolder,  FileName + '_SpkLee_W' + str(Window_Size) + '_NL' + str(Number_Of_Look) +'.tif')

		ApplyLeeFiltering(file,OutputFile, Radius, Number_Of_Look, Ram)
